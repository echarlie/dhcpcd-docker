dhcpcd Docker image
====

for testing dhcpv6 server; best paired with
[pipework](https://github.com/jpetazzo/pipework)

    docker build -t dhcpcd .
    docker run --privledged --rm --name dhcp dhcpcd:latest
    pipework br1 -i eth1 dhcp 0/0
    pipework dummy -i eth2 dhcp 0/0
    pipework dummy -i eth3 dhcp 0/0



