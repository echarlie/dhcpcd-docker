FROM alpine:latest

RUN apk update && apk upgrade && apk add \
    dhcpcd

COPY dhcpcd.conf /etc/dhcpcd.conf

CMD ["dhcpcd","-B"]
